#UC: Desenvolvimento de Software 2015 01
#SimProgramming G3 - Projeto MGM
#Aluno: 1002599 - Octavio Augusto da Silva Oliveira
 
#--------------------------------------------------------------------------------------------------
#                                 SIMULADOR AUTOMATOS
#Serve apenas para iniciar o Simulador de automatos que foi codificado no ficheiro server.py 
#e encontra-se na pasta mgm/simulador 
		
def set_simulador():
	from subprocess import call
	cmd = ComandosSimulador()
	call(cmd.get_cmd() + request.vars.port, cwd=cmd.get_dir(), shell=True)
	
#--------------------------------------------------------------------------------------------------	
#--------------------------------------------------------------------------------------------------	
#                                           ERROS
#Captura de erros externos
class AutomatoError(Exception):
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return repr(self.value)

#Erros gerados localmente		
class ListError(LookupError):
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return repr(self.value)
#--------------------------------------------------------------------------------------------------	
#--------------------------------------------------------------------------------------------------
#                                   LIGACAO SIMULADOR (HTTPD SERVER)				
#Obtem os dados do estado atual do automato, atrvés da interface de comunicacao "socket"	
class SimuladorAutomaton():
	def __init__(self, ip, port):
		self.ip = ip
		self.port = port
	
	#fixa o argumento de consulta	
	def set_question(self, number_automaton):
		self.arg_conslt = "/automato=" + number_automaton
		
	#fixa o argumento de configuracao
	def set_config(self, number_automaton, tipo_automato, quantidade, estados, alertas):
		self.arg_conslt = "/automato="+number_automaton+"tipo="+tipo_automato+get_settings_automato(1)+quantidade+get_settings_automato(2)+estados+get_settings_automato(3)+alertas
	
	#establece a ligacao			
	def connect(self):
		import socket
		import sys  
		s = None
		isInt(self.port)
		if not isInt(self.port):
			raise AutomatoError("O valor da porta do simulador nao inteiro")
		else:
			for res in socket.getaddrinfo(self.ip, int(self.port), socket.AF_UNSPEC, socket.SOCK_STREAM):
				af, socktype, proto, canonname, sa = res
				try:
					s = socket.socket(af, socktype, proto)
				except socket.error as msg:
					s = None
					continue
				try:
					s.connect(sa)
				except socket.error as msg:
					s.close()
					s = None
					continue
				break
			if s is None:
				#Gerada excepção devido a socket.error 
				raise AutomatoError("Erro de ligacao ao automato. socket_error ["+str(msg.args[0]).encode("utf-8")+"]")
			else:
				s.sendall(self.arg_conslt)
				receved = s.recv(1024)
				s.close()
				return receved 
			
#--------------------------------------------------------------------------------------------------	
#--------------------------------------------------------------------------------------------------
#O inicio da aplicacao faz-se no browser: http://127.0.0.1:8000/mgm/simula/ quando o 
#servidor web2py está a correr localmente. A funcao seguinte é chamada a view "index.html" 
def index():
    #response.flash=T("Bem vindo ao MGM!")
    #return dict(message=T(""))
	return dict(date_now=get_today())
	
#envia alertas para a view	
def send_alert(msg):
	response.view = 'simula/send_error.html'		
	return dict(msg_erro=msg)
	
#determina um numero aleatorio
def ran_val(max):
	import random
	return str(random.randint(1, max))
#--------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------
#                                         CONFIGURAR AUTOMATOS
#Esta funcao recebe peido da View para configurar um automato
def configurar_automato():
	#Atomatos disponiveis de 1 a 9
	#Tipos de atomatos de 1 a 3
	
	simulador_automato = SimuladorAutomaton(request.vars.ip, request.vars.port)
	simulador_automato.set_config(request.vars.num_automato, request.vars.tipo, request.vars.quantidade, request.vars.estados, request.vars.alertas)
	try:#Envia resultado para a View
		recebido = simulador_automato.connect()
		return send_alert(get_result_config(request.vars.num_automato, recebido))
	except AutomatoError as msg: 
		return send_alert(msg)
		
#Recebe pedido da View para configurar todos automatos	
def configurar_automato_todos():
	recebido = ""
	#Faz a configuracao aleatoriamente e devolve resultado a View
	for i in range(1, 4):
		simulador_automato = SimuladorAutomaton(request.vars.ip, request.vars.port)
		simulador_automato.set_config(str(i), "1", ran_val(50), ran_val(9), ran_val(9))
		try:
			recev = simulador_automato.connect()
			recebido = recebido + get_result_config(str(i), recev)+ ".\\n "
		except AutomatoError as msg: 
			return send_alert(msg)	
	for i in range(4, 7):
		simulador_automato = SimuladorAutomaton(request.vars.ip, request.vars.port)
		simulador_automato.set_config(str(i), "2", ran_val(50), ran_val(9), ran_val(9))
		try:
			recev = simulador_automato.connect()
			recebido = recebido + get_result_config(str(i), recev)+ ".\\n "
		except AutomatoError as msg: 
			return send_alert(msg)				
	for i in range(7, 10):
		simulador_automato = SimuladorAutomaton(request.vars.ip, request.vars.port)
		simulador_automato.set_config(str(i), "3", ran_val(50), ran_val(9), ran_val(9))
		try:
			recev = simulador_automato.connect()
			recebido = recebido + get_result_config(str(i), recev)+ ".\\n "
		except AutomatoError as msg: 
			return send_alert(msg)	
	return send_alert(recebido)
		
#--------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------
#                                  CONSULTAR AUTOMATOS
#Recebe solicitacao da View para consultar o estado do automato
#Establece ligacao com o automato "class SimuladorAutomaton()" e pede dados do seu estado "recebido"
def consultar():
	simulador_automato = SimuladorAutomaton(request.vars.ip, request.vars.port)
	simulador_automato.set_question(request.vars.automato_sel)
	try:
		recebido = simulador_automato.connect()
	except AutomatoError as msg:#Se ocorreu erro a ligar ao aotomato
		#Envia o erro para a View
		return send_alert(msg)
	#Envia "recebido" para o Model, pede convercao dos dados no formato geral (lista "result") 
	automato = Automaton(recebido, request.vars.automato_sel)
	automato.set_data_receved()
	try: 
		result = automato.get_data()
	except ListError as msg: #Se ocorreu erro na construcao da lista
		return send_alert(msg)
	try:
		#Envia a lista "result" para o Model, para ser inserida na Base de Dados.  
		retorno_bd = insert_reg_atomato_msg(result)
	except AutomatoError as msg:#Se ocorreu erro a inserir na BD 
	#Envia a lista para a View
		response.view = 'simula/send_errorBD.html'		
		return dict(msg_erro=msg, automato_num=result[0], automato_config=result[1], 
					dateini=result[2], timeini=result[3], 
					tempo_total=result[4],tempo_funcionamento=result[5],
					disponibilidade=result[6], total_produzido=result[7], 
					quantidade_produzida_hora=result[8], estado=result[9], 
					alerta=result[10], msg_bd="---")
					
	response.view = 'simula/consultar_automato.html'
	return dict(automato_num=result[0], automato_config=result[1], 
					dateini=result[2], timeini=result[3], 
					tempo_total=result[4],tempo_funcionamento=result[5],
					disponibilidade=result[6], total_produzido=result[7], 
					quantidade_produzida_hora=result[8], estado=result[9], 
					alerta=result[10], msg_bd=retorno_bd)
#--------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------
#                                 CONSULTAR HISTORICO	
#Envia pedido de consulta de historico ao Model, transmite a resposta dada a View 
#Consulta por numero de automato		
def consultar_mensagens_automato():
	row = get_automato_msg_by_automato(request.vars.num_automatoBD)
	return dict(rows=row)
	#return dict(num=row.NumAutomato, dateini=row.DataInicio, TempoTotal=row.TempoTotal,
	#			Disponibildiade=row.Disponibildiade, QtProduzida=row.QtProduzida, 
	#			Estado=get_estado_msg(row.Estado), Alertas=get_alerta_msg(row.Alertas))

#Consulta por data de registo
def mensagens_date():
	row = get_automato_msg_by_date(request.vars.calendarini, request.vars.calendarfim)
	return dict(rows=row)
	
#--------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------
#                                      CONSULTA BASE DADOS	
#Consulta a algumas tabelas da BD					
def display_msg():
	grid = SQLFORM.smartgrid(db.Automato_mensagens)
	return dict(grid=grid)

def display_estado():
	grid = SQLFORM.smartgrid(db.Estados)
	return dict(grid=grid)

def display_alertas():
	grid = SQLFORM.smartgrid(db.Alertas)
	return dict(grid=grid)
	
def display_automatos():
	grid = SQLFORM.smartgrid(db.Automatos)
	return dict(grid=grid)