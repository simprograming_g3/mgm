# -*- coding: utf-8 -*-
import smtplib

mail_de = 'email@gmail.com'
mail_para = 'email@gmail.com'
mail_username = 'email@gmail.com'
mail_password = 'password'


alerta_falha = "\r\n".join([
    "From: " + mail_de,
    "To: " + mail_para,
    "Subject: Falha de uma máquina",
    "",
    "Existe uma falha de uma máquina!\n"
])#.encode('utf-8')


#  Função que envia o email - Funciona para GMAIL
def sendmail(msg):
    server = smtplib.SMTP('smtp.gmail.com:587')
    server.ehlo()
    server.starttls()
    server.login(mail_username, mail_password)
    server.sendmail(mail_de, mail_para, msg)
    server.quit()
