# -------------------------------------------------------------------------
# Grupo 3 SimProgramming - UAb - Desenvolvimento de software
# Classes para query da BD.
# Created on 27-05-2016
# Rev. 0
# Autor: Marco Costa - 1200229
# -------------------------------------------------------------------------

# menu test Automato http://127.0.0.1:8000/MGM/simula/menu_teste.html

# -------------------------------------------------------------------------
# Interacao com DB query de dados
# -------------------------------------------------------------------------

# Configuracao dos automatos
# 1 - quantidade
# 2 - estados
# 3 - alertas
#Obtem settings dos automatos armazenados na BD
def get_settings_automato(cod_id):
    for row in db(db.Config_Automato.Codigo == cod_id).select():
        if row.id is not None:
            return row.Mensagem
        else:
            return ""

#Historico de consultas por automato			
def get_automato_msg_by_automato(auto_id):
	try:
		rows = (db(db.Automato_mensagens.NumAutomato == auto_id)).select()
		return rows
	except Exception, err:
		raise AutomatoError("Base Dados: "+str(err.args[0]).encode("utf-8"))

#Historico de consultas por data
def get_automato_msg_by_date(data_incio, data_fim):
	import datetime	
	try:
		#Controlo de datas
		dti = datetime.datetime.strptime(data_incio, '%Y-%m-%d').date()
		dtf = datetime.datetime.strptime(data_fim, '%Y-%m-%d').date()
		today = datetime.datetime.strptime(get_today(), '%Y-%m-%d').date() 
		if dti > dtf or dti > today:
			return[]
		rows = (db(db.Automato_mensagens.DataConsulta >= data_incio and db.Automato_mensagens.DataConsulta <= data_fim)).select()
		return rows
	except Exception, err:
		raise AutomatoError("Base Dados: "+str(err.args[0]).encode("utf-8"))
		
#Obtem o alerta atraves do seu codigo 			
def get_alerta_msg(cod_id):
    for row in db(db.Alertas.Codigo == cod_id).select():
        if row.id is not None:
            return row.Alerta
        else:
            return ""   

#Obtem o estado atraves do seu codigo
def get_estado_msg(cod_id):
    for row in db(db.Estados.Codigo == cod_id).select():
        if row.id is not None:
            return row.Estado
        else:
            return ""			
