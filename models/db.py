# -*- coding: utf-8 -*-

# -------------------------------------------------------------------------
# Grupo 3 SimProgramming - UAb - Desenvolvimento de software
# Base dados DAL
# Created on 18-04-2016
# Rev. 0
# Autor: Marco Costa - 1200229
# -------------------------------------------------------------------------

if request.global_settings.web2py_version < "2.14.1":
    raise HTTP(500, "Requires web2py 2.13.3 or newer")

# -------------------------------------------------------------------------
# if SSL/HTTPS is properly configured and you want all HTTP requests to
# be redirected to HTTPS, uncomment the line below:
# -------------------------------------------------------------------------
# request.requires_https()

# -------------------------------------------------------------------------
# app configuration made easy. Look inside private/appconfig.ini
# -------------------------------------------------------------------------
from gluon.contrib.appconfig import AppConfig

# -------------------------------------------------------------------------
# once in production, remove reload=True to gain full speed
# -------------------------------------------------------------------------
myconf = AppConfig(reload=True)

if not request.env.web2py_runtime_gae:
    # ---------------------------------------------------------------------
    # if NOT running on Google App Engine use SQLite or other DB
    # ---------------------------------------------------------------------
    db = DAL(myconf.get('db.uri'),
             pool_size=myconf.get('db.pool_size'),
             migrate_enabled=myconf.get('db.migrate'),
             check_reserved=['all'])
else:
    # ---------------------------------------------------------------------
    # connect to Google BigTable (optional 'google:datastore://namespace')
    # ---------------------------------------------------------------------
    db = DAL('google:datastore+ndb')
    # ---------------------------------------------------------------------
    # store sessions and tickets there
    # ---------------------------------------------------------------------
    session.connect(request, response, db=db)
    # ---------------------------------------------------------------------
    # or store session in Memcache, Redis, etc.
    # from gluon.contrib.memdb import MEMDB
    # from google.appengine.api.memcache import Client
    # session.connect(request, response, db = MEMDB(Client()))
    # ---------------------------------------------------------------------

# -------------------------------------------------------------------------
# by default give a view/generic.extension to all actions from localhost
# none otherwise. a pattern can be 'controller/function.extension'
# -------------------------------------------------------------------------
response.generic_patterns = ['*'] if request.is_local else []
# -------------------------------------------------------------------------
# choose a style for forms
# -------------------------------------------------------------------------
response.formstyle = myconf.get('forms.formstyle')  # or 'bootstrap3_stacked' or 'bootstrap2' or other
response.form_label_separator = myconf.get('forms.separator') or ''

# -------------------------------------------------------------------------
# (optional) optimize handling of static files
# -------------------------------------------------------------------------
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'

# -------------------------------------------------------------------------
# (optional) static assets folder versioning
# -------------------------------------------------------------------------
# response.static_version = '0.0.0'

# -------------------------------------------------------------------------
# Here is sample code if you need for
# - email capabilities
# - authentication (registration, login, logout, ... )
# - authorization (role based authorization)
# - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
# - old style crud actions
# (more options discussed in gluon/tools.py)
# -------------------------------------------------------------------------

from gluon.tools import Auth, Service, PluginManager

# host names must be a list of allowed host names (glob syntax allowed)
auth = Auth(db, host_names=myconf.get('host.names'))
service = Service()
plugins = PluginManager()

# -------------------------------------------------------------------------
# create all tables needed by auth if not custom tables
# -------------------------------------------------------------------------
auth.define_tables(username=False, signature=False)

# -------------------------------------------------------------------------
# configure email
# -------------------------------------------------------------------------
mail = auth.settings.mailer
mail.settings.server = 'logging' if request.is_local else myconf.get('smtp.server')
mail.settings.sender = myconf.get('smtp.sender')
mail.settings.login = myconf.get('smtp.login')
mail.settings.tls = myconf.get('smtp.tls') or False
mail.settings.ssl = myconf.get('smtp.ssl') or False

# -------------------------------------------------------------------------
# configure auth policy
# -------------------------------------------------------------------------
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True

# -------------------------------------------------------------------------
# Define your tables below (or better in another model file) for example
#
# >>> db.define_table('mytable', Field('myfield', 'string'))
#
# Fields can be 'string','text','password','integer','double','boolean'
#       'date','time','datetime','blob','upload', 'reference TABLENAME'
# There is an implicit 'id integer autoincrement' field
# Consult manual for more options, validators, etc.
#
# More API examples for controllers:
#
# >>> db.mytable.insert(myfield='value')
# >>> rows = db(db.mytable.myfield == 'value').select(db.mytable.ALL)
# >>> for row in rows: print row.id, row.myfield
# -------------------------------------------------------------------------

# -------------------------------------------------------------------------
# after defining tables, uncomment below to enable auditing
# -------------------------------------------------------------------------
# auth.enable_record_versioning(db)

# -------------------------------------------------------------------------
# Tabela de registo de cada automato
db.define_table('Automatos',
                Field('NumeroSerie', 'string'),
                Field('Modelo', 'string'),
                Field('Marca', 'string'),
                Field('Localizacao', 'string'))
				
# Tabela de alertas
db.define_table('Alertas',
                Field('Codigo', 'integer'),
                Field('Alerta', 'string'))
				
# Tabela de estados
db.define_table('Estados',
                Field('Codigo', 'integer'),
                Field('Estado', 'string'))
				
# Tabela de registo de mensagens emitidas por cada automato
db.define_table('Automato_mensagens',
                Field('NumAutomato', db.Automatos),
				Field('TipoAutoma', 'integer'),
                Field('DataInicio', 'date'),
                Field('TempoInicio', 'time'),
                Field('TempoTotal', 'integer'),
                Field('TempoOperacao', 'integer'),
                Field('Disponibildiade', 'double'),
                Field('QtProduzida', 'integer'),
				Field('QtProduzidaHora', 'integer'), 
				Field('Estado', db.Estados),
				Field('Alertas', db.Alertas),
				Field('DataConsulta', 'date'))
				
# Tabela de registo de grupos de atores
db.define_table('Grupos',
                Field('Numero', 'integer'),
                Field('Nome', 'string'))
					 
# Tabela de registo de utilziadores (tecnicos, gestores, etc)
db.define_table('Utilizadores',
                Field('Numero', 'integer'),
                Field('Nome', 'string'),
                Field('Email', 'string'),
                Field('Telefone', 'integer'),
                Field('Estado', 'string'),
                Field('Grupo', db.Grupos),
                Field('Observacoes', 'string'))

# Tabela de configuracao de automatos
db.define_table('Config_Automato',
                Field('Codigo', 'integer'),
                Field('Mensagem', 'string'))

# -------------------------------------------------------------------------