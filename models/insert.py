# -------------------------------------------------------------------------
# Grupo 3 SimProgramming - UAb - Desenvolvimento de software
# Classes para udate de BD.
# Created on 18-04-2016
# Rev. 0
# Autor: Marco Costa - 1200229
# -------------------------------------------------------------------------

# menu test Automato http://127.0.0.1:8000/MGM/simula/menu_teste.html

# -------------------------------------------------------------------------
# Interacao com DB insercao de dados

# -------------------------------------------------------------------------
from applications.mgm.controllers.simula import AutomatoError


def registar_automato(numero_serie, modelo, marca, localizacao):
    db.Automatos.insert(NumeroSerie=numero_serie,
                        Modelo=modelo,
                        Marca=marca,
                        Localizacao=localizacao)

def inserir_estado(estado):
    db.Estados.insert(Codigo=id,
                      Estado=estado)

def inserir_alerta(alerta):
    db.Alertas.insert(Codigo=id,
                      Alerta=alerta)


def inserir_grupo(nome):
    db.Grupos.insert(Numero=id,
                     Nome=nome)


def registar_utilizador(nome, email, telefone, estado, obs):
    db.Utilizadores.insert(Numero=id,
                           Nome=nome,
                           Email=email,
                           Telefone=telefone,
                           Estado=estado,
                           Observacoes=obs)

def inserir_mensagem(num, tipo, data_ini, tempo_ini, tempo_total, tempo_operacao, disponib, qt_prod, qt_prod_hora, estad, alert, data):
    try:
        ret = db.Automato_mensagens.validate_and_insert(NumAutomato=num,
                        TipoAutoma=tipo,
                        DataInicio=data_ini,
                        TempoInicio=tempo_ini,
                        TempoTotal=tempo_total,
                        TempoOperacao=tempo_operacao,
                        Disponibildiade=disponib,
                        QtProduzida=qt_prod,
                        QtProduzidaHora=qt_prod_hora,
                        Estado=estad,
                        Alertas=alert,
						DataConsulta=data)
        return ret.id
    except Exception, err:#deteta erro de BD
        raise AutomatoError("Base Dados: "+str(err.args[0]).encode("utf-8"))