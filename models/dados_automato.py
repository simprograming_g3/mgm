#Dados dos automatos
#UC: Desenvolvimento de Software 2015 01
#SimProgramming G3 - Projeto MGM
#Aluno: 1002599 - Octavio Augusto da Silva Oliveira

#--------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------
#                     CLASSES E FUNCOES AUXILIARES PARA MANIPULAR OS DADOS RECEBIDOS			

#Classe auxiliar para dividir a string enviada pelo simulador, em funcao de um separador		
class Campos():
	def set_structure(self, ini, separator):
		self.ini = ini
		self.separator = separator  
		
	def get_str_position(self, data):
		return data.find(self.separator)
		
	def get_values(self, position, receved):
		ini = self.ini 
		for x in range(0, position):
			end = ini + self.get_str_position(receved[ini:])
			if end < ini:
				result = receved[ini:]
			else:
				result = receved[ini:end]
			ini = end + 1
		return result
		
	def get_position(self, position, receved):
		count = 0
		pos = 0
		data = receved
		while (count < position + 1):
			k = data.find(self.separator) + 1
			pos = pos + k
			data = data[k:]
			count = count + 1
		return pos
		
#Classe para determinar o tipo de automato	
class TiposAutomatos():
	def set_data(self, receved):
		self.receved = receved
		
	def get_tipo_automato(self, arg, count):
		try:
			k = self.receved.find(arg) + len(arg)
			return int(self.receved[k:k+count])
		except ValueError:
			return -1
		
		#Estrutura da string que deve ser entregue a classe Automato: numero de automato; tipo; dia e hora inicio; tempo total; 
		#tempo funcionamento disponibilidade; total produzido; quantidade produzida hora; estado; alerta
		#Esta clase tem definidos 3 tipos de automatos: 1, 2 e 3
		#Os automatos tipo 1 enviam uma string identica a esta: Automato5;Tipo1;2016-03-24;02:46:19;720;699;97;699;1;2;2
		#Os automatos tipo 2 enviam uma string identica a esta: Automato5;Tipo2;2016-03-24;02:46:19;...;699;97;699;1;2;2 (;... nao envia)
		#Os automatos tipo 3 enviam uma string identica a esta: Automato 5;Tipo 3;2016-03-24;02:46:19;...;699;97;...;1;2;2 (;... nao envia)
		#Em 2 temos de inserir o campo tempo total
		#Em 3 temos de inserir o campo tempo total e o total produzido		
	def get_data(self, tipo):
		if tipo == 1:
			#Nao faz qualqer alteracao. A string enviada pelo automato esta conforme a geral
			return self.receved
		elif tipo == 2:
			campos = Campos()
			campos.set_structure(36, ";")
			tempo_total = 100 * int(campos.get_values(1, self.receved)) / int(campos.get_values(2, self.receved)) 
			#Inseriri Tempo total
			self.receved = self.receved[:36]+str(tempo_total)+";"+self.receved[36:]
			return self.receved
		elif tipo == 3:
			campos = Campos()
			campos.set_structure(36, ";")
			#tempo total = tempo funcionamento / disponibilidade
			tempo_total = 100 * int(campos.get_values(1, self.receved)) / int(campos.get_values(2, self.receved))
			#total produzido = tempo funcionamento * quantidade produzida hora
			total_produzido = int(campos.get_values(1, self.receved)) * int(campos.get_values(3, self.receved))  
			#Inseriri Tempo total
			self.receved = self.receved[:36]+str(tempo_total)+";"+self.receved[36:]
			pos = campos.get_position(6, self.receved) 
			#Inseriri Total produzido
			self.receved = self.receved[:pos]+str(total_produzido)+";"+self.receved[pos:]
			return self.receved 	
		else: return ""
		
#A calsse TiposAutomatos deve gerar uma string com a estrutura seguinte:
#numero de automato; tipo; dia e hora inicio; tempo total; tempo funcionamento
#disponibilidade; total produzido; quantidade produzida hora; estado; alerta	
class AutomatoGeral():
	def set_data(self, tipo, separator, receved):
		self.receved = receved
		self.ini = 0
		if tipo > 0 and tipo < 4:
			self.ini = 36
		self.separator = separator
		
	def get_str_position(self, data):
		return data.find(self.separator)
	
	def get_num(self):
		return self.receved[8:9]	
	
	def get_tipo(self):
		return self.receved[14:15]
	
	def get_date(self):
		return self.receved[16:26]
	
	def get_time(self):
		return self.receved[27:35]
		
	def get_values(self, position):
		campos = Campos()
		campos.set_structure(self.ini, self.separator)
		return campos.get_values(position, self.receved)

#--------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------
#     CONVERTER DADOS DOS DIFERENTES AUTOMATOS EM LISTA COM ESTRUTURA IGUAL PARA TODOS AUTOMATOS 					
#Classe que devolve uma lista com os dados automato	
class Automaton():
	receved = ""
	automaton_number = ""
	automaton_type = "-1"
	dateini = ""
	timeini = ""
	total_time = ""
	time_operation = ""
	availability = ""
	total_production = ""		
	quantity_produced_hour = ""
	state = ""
	alerts = ""
	arg_conslt = ""
	tipo_automato = -1
	
	def __init__(self, receved, automaton_number):	
		self.receved = receved
		self.automaton_number = automaton_number
		
	def set_data_receved(self):
		automato = TiposAutomatos()
		automato.set_data(self.receved)
		#Procura o tipo de automato
		self.tipo_automato = automato.get_tipo_automato("Tipo", 1) 
		if(self.tipo_automato > 0 and self.tipo_automato < 4):
			#Converte os dados do tipo de automato especifico nos dados do automato geral
			dados_automato = AutomatoGeral()
			dados_automato.set_data(self.tipo_automato, ";", automato.get_data(self.tipo_automato))
			self.automaton_number = dados_automato.get_num()
			self.automaton_type = dados_automato.get_tipo()				
			self.dateini = dados_automato.get_date()
			self.timeini = dados_automato.get_time() 
			self.total_time = dados_automato.get_values(1)
			self.time_operation = dados_automato.get_values(2)
			self.availability = dados_automato.get_values(3)
			self.total_production = dados_automato.get_values(4)			
			self.quantity_produced_hour = dados_automato.get_values(5)
			self.state = dados_automato.get_values(6) 
			self.alerts = dados_automato.get_values(7)
		else:
			self.automaton_type = self.receved
	
	
	#Devolve os dados recebidos numa lista. 
	#Se o parametro nao e valido indica-o no fim da lista, exemplo:[num automato, Automato desconhecido.] 
	def get_data(self):
		list = []
		#Verifica se o numero de automato e valido
		if not isInt(self.automaton_number):
			raise ListError("Automato desconhecido.")
		else:
			if int(self.automaton_number) < 1 or int(self.automaton_number) > 9:
				raise ListError("Autómato desconhecido!")#Se ocorreu erro 
		list.append(self.automaton_number)
		if(self.tipo_automato == -1):
			raise ListError("Autómato desconhecido!")
		if(self.tipo_automato == 0):
			raise ListError("Automato "+self.automaton_number+" nao configurado!")
		list.append(str(self.tipo_automato))
		list.append(self.dateini)
		list.append(self.timeini)
		#Verifica se sao inteiros: total_time, time_operation, availability, total_production, quantity_produced_hour, state e alerts    
		if not isInt(self.total_time):
			raise ListError("O total de tempo recebido invalido.")
		list.append(self.total_time)
		if not isInt(self.time_operation):
			raise ListError("O total de atividade recebido invalido.")
		list.append(self.time_operation)
		if not isInt(self.availability):
			raise ListError("A disponibilidade recebida invalido.")
		list.append(self.availability)
		if not isInt(self.total_production):
			raise ListError("A produção recebida invalido.")
		list.append(self.total_production)
		if not isInt(self.quantity_produced_hour):
			raise ListError("A produção por hora recebida invalido.")
		list.append(self.quantity_produced_hour)
		if not isInt(self.state):
			raise ListError("O codigo de estado recebido invalido.")
		list.append(self.state)
		if not isInt(self.alerts):
			raise ListError("O codigo de alerta recebido invalido.")
		list.append(self.alerts)
		return list

#--------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------
#                    ANALISAR DADOS RECEBIDOS PARA DETERMINAR CONFIGURACAO DO AUTOMATO	
#Obetem a configuracao do automato
def get_result_config(automaton_number, arg):
	if arg  == "Nao config":
		return "Automato " + automaton_number + " não configurado!"
	else:
		if arg  == "Automato " + automaton_number + " configurado":
			return arg 
		else: 
			return "Não foi possível configurar o autómato " + automaton_number 
			
#--------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------
#                                      HISTORICO
#Insere registo na base dados para historico.
def insert_reg_atomato_msg(result):
	list = []
	list.append(result[0]) 
	"""pos_erro = len(result) 
	if pos_erro < 11:
		erro = result[pos_erro-1]
		for i in range(pos_erro, 11):
			list.append(0)
	else:"""
	for i in range(1, 11):
		list.append(result[i])	
	return inserir_mensagem(list[0], list[1], list[2], list[3], list[4],list[5], list[6], 
					list[7],list[8], list[9], list[10], get_today())

#--------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------
#                            CLASSES E FUNCOES AUXILIARES
import os
import platform
class ComandosSimulador():
	def get_cmd(self):
		if platform.system() == "Windows":
			return "start cmd /K py applications\mgm\simulador\server.py "
		else:
			return "python server.py " 
	
	def get_dir(self):
		if platform.system() == "Windows":
			dir = os.getcwd()
		else:	
			dir = os.getcwd()+"/applications/mgm/simulador"
		return dir

#Verificar se e inteiro
def isInt(s):
	try: 
		int(s)
		return True
	except ValueError:
		return False

#Obtem a data atual para inserir no registo de mensagens.		
def get_today():
	from time import gmtime, strftime
	return strftime("%Y-%m-%d", gmtime())					