#!/usr/bin/python
""" Simulador Automatos CLIENTE
 UC: Desenvolvimento de Software 2015 01
 SimProgramming G3 - Projeto MGM
 Aluno: 1002599 - Octavio Augusto da Silva Oliveira

 ip; porto; numero automato; quantidade max por hora; max estados; max alertas
 >python client.py localhost 8001 /automato=5tipo=1quantidade=20estados=5alertas=5
""" 

import socket
import sys

# HOST = 'octaviooli.ddns.net'
HOST = sys.argv[1]  
PORT = sys.argv[2]            
s = None
for res in socket.getaddrinfo(HOST, PORT, socket.AF_UNSPEC, socket.SOCK_STREAM):
    af, socktype, proto, canonname, sa = res
    try:
        s = socket.socket(af, socktype, proto)
    except socket.error as msg:
        s = None
        continue
    try:
        s.connect(sa)
    except socket.error as msg:
        s.close()
        s = None
        continue
    break
if s is None:
    print 'nao foi possivel abrir o socket'
    sys.exit(1)
s.sendall(sys.argv[3])
data = s.recv(1024)
s.close()
print repr(data)
