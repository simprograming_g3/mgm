""" Simulador Automatos SERVIDOR
 UC: Desenvolvimento de Software 2015 01
 SimProgramming G3 - Projeto MGM
 Aluno: 1002599 - Octavio Augusto da Silva Oliveira

 >python server.py 8001 (argumento porto)
 O programa termina com CTRL+BREAK
""" 
# !/usr/bin/python
import socket
import sys
import random
from datetime import datetime, date, timedelta
import math

class Config():
    def set_config(self, enviado):
        self.enviado = enviado
        # /automato=1
        self.num_automato = self.enviado[10:11]
        # tipo=1
        self.tipo = self.enviado[16:17]
        # quantidade=1
        i = self.enviado.find("estados") 
        self.max_quantidade_hora = self.enviado[28:i]
        j = self.enviado.find("alertas")
        self.max_estado = self.enviado[i+8:j] 
        self.max_alerta = self.enviado[j+8:]
        
    def get_num_automato(self):
        if (self.num_automato == ""):
            return -1
        else:
            return int(self.num_automato) 
    
    def    get_tipo(self):
        if (self.tipo == ""):
            return -1
        else:
            return int(self.tipo)
        
    def    get_max_quantidade_hora(self):
        if (self.max_quantidade_hora == ""):
            return -1
        else:
            return int(self.max_quantidade_hora)
        
    def    get_max_estado(self):
        if (self.max_estado == ""):
            return -1
        else:
            return int(self.max_estado)
        
    def    get_max_alerta(self):
        if (self.max_alerta == ""):
            return -1
        else:
            return int(self.max_alerta)
        
    def is_config(self):
        if self.enviado.find("tipo") > 0:
            return 1
        else: 
            if self.enviado.find("automato") > 0:
                return 0
            else:
                return -1
    
class Automato():
    def set_config(self, tipo, quant_hora, estado, alerta):
        self.tipo = tipo
        self.quant_hora = quant_hora
        self.estado = estado
        self.alerta = alerta
        self.inicio = datetime.now() - timedelta(days=30)    
        
    def get_estado(self, tipo):
        data_hora = str(self.inicio)
        if self.quant_hora > 0 and self.estado > 0 and self.alerta > 0:  
            quantidade = random.randint(1, self.quant_hora)  # quantidade produzida por hora
            estados = random.randint(1, self.estado)
            alertas = random.randint(1, self.alerta)
            # considera que os automatos tem inicio 240 horas antes da hora inicial
            duracao_total = abs(datetime.now() - self.inicio).total_seconds() / 3600.0
            fator = random.uniform(0.5, 1) 
            duracao = int(fator * duracao_total) # tempo de funcionamento
            fator = int(fator*100)  # disponibilidade
            quantidade_total_produzida = quantidade * duracao  # Quantidade total produzida
            duracao_total = int(duracao_total)
            dados_enviar = data_hora[:10] + ";" + data_hora[11:19] + ";"
            if tipo == 1:
                dados_enviar += str(duracao_total) + ";" + str(duracao) + ";" 
                dados_enviar += str(fator) + ";" + str(quantidade_total_produzida) + ";"  
                dados_enviar += str(quantidade) + ";" + str(estados)+";" + str(alertas)            
            else:
                if tipo == 2:
                    dados_enviar += str(duracao) + ";" 
                    dados_enviar += str(fator) + ";" + str(quantidade_total_produzida) + ";"  
                    dados_enviar += str(quantidade) + ";" + str(estados)+";" + str(alertas)
                else:
                    dados_enviar += str(duracao) + ";" 
                    dados_enviar += str(fator) + ";"  
                    dados_enviar += str(quantidade) + ";" + str(estados)+";" + str(alertas)
            return dados_enviar 
        else:
            return "nao configurado"
    
    def get_tipo(self):
        return self.tipo 
            
        
class Simulador():
    def __init__ (self):
        self.automato = []

    def set_automato(self):
        self.automato.append(Automato())
        
    def check_config(self):
        if self.enviado.find(self.teste) > 0:
            if self.enviado.find("quant_hora") > 0:
                return True
        else:
            return False

configuracao = Config()
# configuracao.set_config("");
 
simulador = Simulador()
for k in range(0, 10):
    simulador.set_automato()
    simulador.automato[k].set_config(0,0,0,0)

    
HOST = None               
PORT = int(sys.argv[1])              
s = None
for res in socket.getaddrinfo(HOST, PORT, socket.AF_UNSPEC,
                              socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
    af, socktype, proto, canonname, sa = res
    try:
        s = socket.socket(af, socktype, proto)
    except socket.error as msg:
        s = None
        continue
    try:
        s.bind(sa)
        s.listen(1)
    except socket.error as msg:
        s.close()
        s = None
        continue
    break
if s is None:
    print ('Nao foi possivel abrir o socket')
    sys.exit(1)
while 1:
    conn, addr = s.accept()
    print ('Concecao '), addr
    while 1:
        recebido = conn.recv(1024)
        # print recebido
        enviar = "Erro"
        configuracao = Config()
        configuracao.set_config(recebido)
        num_automato = configuracao.get_num_automato()
        configura = configuracao.is_config()
        if num_automato != -1:
            if configura == 1:  # Faz a configuracao
                max_quantidade_hora = configuracao.get_max_quantidade_hora()
                max_estado = configuracao.get_max_estado()
                max_alerta = configuracao.get_max_alerta()
                tipo = configuracao.get_tipo()
                if tipo is 1:
                    simulador.automato[num_automato].set_config(tipo, max_quantidade_hora, max_estado, max_alerta)
                    enviar = "Automato "+ str(num_automato) +" configurado"
                else:
                    if tipo is 2:  # fixa o maximo de alertas em 10
                        simulador.automato[num_automato].set_config(tipo,max_quantidade_hora,max_estado,10)
                        enviar = "Automato " + str(num_automato) + " configurado"
                    else:
                        if tipo is 3:  # fixa o maximo de alertas e sstados em 10
                            simulador.automato[num_automato].set_config(tipo, max_quantidade_hora, 10, 10)
                            enviar = "Automato " + str(num_automato) + " configurado"
            else:
                if configura == 0:  # Faz a consulta
                    tipo_aut = simulador.automato[num_automato].get_tipo()
                    enviar = "Automato" + str(num_automato) + ";" + "Tipo" + str(tipo_aut) + ";"
                    enviar += simulador.automato[num_automato].get_estado(tipo_aut)
                    # print enviar
        # else: print "nao"
        # print data
        if not recebido: break
        conn.send(enviar)

conn.close()
