ESTADOS dos aut?matos
Grupo data hora = momento de inicio do aut?mato 
(No momento da configura??o estabelece que o aut?mato foi iniciado 30 dias antes.) 
tempo total = tempo atual - inicio
tempo funcionamento = tempo total * disponibilidade (disponibilidade varia de 0 a 1 e gerado aleatoriamente) 
total produzido = tempo funcionamento * quantidade produzida hora
quantidade produzida hora = numero gerado aleatoriamente entre 1 e quant_hora 
quant_hora e numero dado na configura??o 
O estado e alerta s?o uns n?meros gerados aleatoriamente entre 1 e respetivamente estados e alertas (n?meros dados na configura??o)   

DEVOLVE 3 tipos de estados conforme o tipo de aut?mato.
Utiliza-se o programa client.py. O aut?mato s? transmite o seu estado quando esta configurado.
>python client.py localhost 8001 /automato=7
Tipo 1
>Automato 5;Tipo 1;2016-03-24;02:46:19;720;699;97;699;1;2;2
numero de aut?mato; tipo; dia e hora inicio; tempo total; tempo funcionamento
disponibilidade; total produzido; quantidade produzida hora; estado; alerta
Tipo 2
>Automato 5;Tipo 2;2016-03-24;02:46:19;...;699;97;699;1;2;2 (... significa que n?o envia)
numero de aut?mato; tipo; dia e hora inicio; tempo funcionamento
disponibilidade; total produzido; quantidade produzida hora; estado; alerta
Tempo total = tempo funcionamento + tempo funcionamento (100-disponibilidade) 
Tipo 3
>Automato 5;Tipo 3;2016-03-24;02:46:19;...;699;97;...;1;2;2 (... significa que n?o envia)
numero de automato; tipo; dia e hora inicio; tempo funcionamento
disponibilidade; total produzido; quantidade produzida hora; estado; alerta
total produzido = tempo funcionamento * quantidade produzida hora 

CONFIGURACAO
Utiliza-se o programa client.py com 3 argumentos (ip porto configura??o)
Aceita 3 tipos de configura??o, conforme o tipo de aut?mato
Tipo 1
>python client.py localhost 8001 /automato=6tipo=2quantidade=20estados=5alertas=5
>Automato 6 configurado
Tipo 2 (Por omiss?o tem de m?ximo de alertas 10)
>python client.py localhost 8001 /automato=6tipo=2quantidade=20estados=5
>Automato 6 configurado
Tipo 2 (Por omiss?o tem de m?ximos de alertas e estados 10)
>python client.py localhost 8001 /automato=6tipo=2quantidade=20
>Automato 6 configurado